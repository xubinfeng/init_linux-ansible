# init_linux-ansible

#### 介绍
ansible 优化脚本，只支持centos-7

#### 注意事项
注意command的记录日志，是否需要关闭等操作；默认为false，记录command日志

#### demo1
```
ansible-playbook init_linux.yml -e "run=node1"
```

### demo host
```
[master]
master1 ansible_ssh_host=192.168.50.121 host_ip=192.168.50.121 hostname=m001
master2 ansible_ssh_host=192.168.50.122 host_ip=192.168.50.122 hostname=m002
master3 ansible_ssh_host=192.168.50.123 host_ip=192.168.50.123 hostname=m003



[node]
node1 ansible_ssh_host=192.168.50.131 host_ip=192.168.50.131 hostname=n001
node2 ansible_ssh_host=192.168.50.132 host_ip=192.168.50.132 hostname=n002
node3 ansible_ssh_host=192.168.50.133 host_ip=192.168.50.133 hostname=n003
```
